package com.example.practica02s2_java;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    //Spinner sp;
    ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<ItemData> list = new ArrayList<>();
        list.add(new ItemData(getString(R.string.itemFrappses),getString(R.string.msgFrapsses),R.drawable.categorias));
        list.add(new ItemData(getString(R.string.itemAgradecimiento),getString(R.string.msgAgradecimiento),R.drawable.agradecimiento));
        list.add(new ItemData(getString(R.string.itemAmor),getString(R.string.msgAmor),R.drawable.corazon));
        list.add(new ItemData(getString(R.string.itemNewyear),getString(R.string.msgNewYear), R.drawable.nuevo));
        list.add(new ItemData(getString(R.string.itemCanciones),getString(R.string.msgCanciones),R.drawable.canciones));

        lv =(ListView) findViewById(R.id.listview1);
        ListViewAdapter adapter = new ListViewAdapter(this, R.layout.spinner_layout, list);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(adapterView.getContext(), "Seleccionado: " + ((ItemData) adapterView.getAdapter().getItem(i)).getTextCategoria(), Toast.LENGTH_SHORT).show();
            }
        });

        //lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        //    @Override
        //    public void OnItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        //        Toast.makeText(adapterView.getContext(),getString(R.string.msgSeleccionado).toString() +" "+((ItemData) adapterView.getItemAtPosition(i)).getTextCategoria(), Toast.LENGTH_SHORT).show();
        //    }
        //});
    }
}